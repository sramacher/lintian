Tag: diff-contains-svk-commit-file
Severity: warning
Check: cruft
Explanation: The Debian diff or native package contains an
 <code>svk-commitNNN.tmp</code>, almost certainly a left-over from a failed
 svk commit by the Debian package maintainer.
