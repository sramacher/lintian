Tag: diff-contains-svn-commit-file
Severity: warning
Check: cruft
Explanation: The Debian diff or native package contains an
 <code>svn-commit(.NNN).tmp</code>, almost certainly a left-over from a failed
 Subversion commit by the Debian package maintainer.
