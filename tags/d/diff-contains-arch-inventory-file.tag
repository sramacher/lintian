Tag: diff-contains-arch-inventory-file
Severity: warning
Check: cruft
Explanation: The Debian diff or native package contains an
 <code>.arch-inventory</code> file. This is Arch metadata that should
 normally not be distributed.
