Tag: wrong-vcs-location-for-papt
Severity: error
See-Also: PAPT policy
Check: fields/vcs
Explanation: This package is maintained within the Python Applications Packaging Team
 (PAPT) and as such, its VCS should live in Salsa under
 <code>https://salsa.debian.org/python-team/applications/</code>.
 .
 This is not currently the case and the package's VCS should be migrated to the
 proper location.
