Tag: setuid-binary
Severity: warning
Check: files/permissions
Explanation: The file is tagged SETUID. In some cases this is intentional, but in
 other cases this is a bug. If this is intentional, please add a Lintian
 override to document this fact.
